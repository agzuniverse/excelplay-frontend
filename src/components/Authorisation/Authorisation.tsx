import React, { useEffect } from "react";
import configs from '../../config/auth_config';
const config = configs();

const Authorisation = () => {
  useEffect(() => {
    const receiveMessage = (event: any) => {
      if (event.data === null) {
        window.location.assign(
          `${config.AUTH_ROOT}auth/login?redirect_to=${window.location.origin}`
        );
      }
    }
    window.addEventListener("message", receiveMessage);
  }, []);

  return (
    <iframe
      title="authorize"
      style={{ display: "none" }}
      id="content"
      src={config.AUTH_ROOT + "auth/authorize"}
      width="0"
      height="0"
    ></iframe>
  );
};

export default Authorisation;
