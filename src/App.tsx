import React, { Suspense } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import Home from './pages/Home/Home';
import Spinner from './components/common/Spinner/Spinner';
import './App.scss';

import Authorisation from './components/Authorisation/Authorisation';

const App = () => {
  return (
    <div className="main-background">
      <Suspense fallback={<Spinner />}>
        <Router>
          <Route exact path="/" component={Home} />
          <Route exact path="/auth" component={Authorisation} />
        </Router>
      </Suspense>
    </div>
  );
};

export default App;